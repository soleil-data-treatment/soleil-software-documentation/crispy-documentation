# Aide et ressources de Crispy pour Synchrotron SOLEIL

[<img src="https://software.pan-data.eu/cache/7/a/2/1/5/7a215157a6d405f1dfdeadea982035a1f3e85dea.png" width="100"/>](http://www.esrf.eu/computing/scientific/crispy/index.html)

## Résumé

- calculs multiplets
- Open source

## Sources

- Code source: https://github.com/mretegan/crispy/releases
- Documentation officielle: http://www.esrf.eu/computing/scientific/crispy/index.html

## Navigation rapide

| Tutoriaux | Page pan-data |
| - | - |
| [Tutoriel d'installation officiel](http://www.esrf.eu/computing/scientific/crispy/installation.html) | [Documentation pan-data du logiciel](https://software.pan-data.eu/software/138/crispy) |
| [Tutoriaux officiels](http://www.esrf.eu/computing/scientific/crispy/tutorials/index.html) | |

## Installation

- Systèmes d'exploitation supportés: Windows, Linux, MacOS, Machine virtuelle
- Installation: Facile (tout se passe bien)

## Format de données

- en entrée: aucun
- en sortie: fichiers textes et images
- sur un disque dur
